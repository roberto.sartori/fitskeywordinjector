/*
 * Script to change a keyword value in a set of FITS files.
 */
// SET THE FITs HEADER KEYWORD WE WANT TO EXTRACT AND ADD TO THE FILE NAME
let keywordName = "CCD-TEMP";

// TRUE: keyword + value is added at the beginning of the file name
// FALSE: keyword + value is added at the end of the file name
let addAsPrefix = false;

// TRUE: the value is used as a numerical value
let parseAsNumber = true;

// IF the value is a numerical value then this is the precision the value will
// be be rounded to
let precision = 0.1;

// TRUE if you want to rename the file instead of creating a new one
let renameOriginals = true;

// ----------------------------------------------------------------------------

/*
 * FileList
 *
 * Recursively search a directory tree for all existing files with the
 * specified file extensions.
 */
function FileList(dirPath, extensions, verbose) {
   /*
    * Regenerate this file list for the specified base directory and file
    * extensions.
    */
   this.regenerate = function (dirPath, extensions, verbose) {
      // Security check: Do not allow climbing up a directory tree.
      if (dirPath.indexOf("..") >= 0)
         throw new Error("FileList: Attempt to redirect outside the base directory: " + dirPath);

      // The base directory is the root of our search tree.
      this.baseDirectory = File.fullPath(dirPath);
      if (this.baseDirectory.length == 0)
         throw new Error("FileList: No base directory has been specified.");

      // The specified directory can optionally end with a separator.
      if (this.baseDirectory[this.baseDirectory.length - 1] == '/')
         this.baseDirectory.slice(this.baseDirectory.length - 1, -1);

      // Security check: Do not try to search on a nonexisting directory.
      if (!File.directoryExists(this.baseDirectory))
         throw new Error("FileList: Attempt to search a nonexistent directory: " + this.baseDirectory);

      // If no extensions have been specified we'll look for all existing files.
      if (extensions == undefined || extensions == null || extensions.length == 0)
         extensions = [''];

      if (verbose) {
         console.writeln("<end><cbr><br>==> Finding files from base directory:");
         console.writeln(this.baseDirectory);
      }

      // Find all files with the required extensions in our base tree recursively.
      this.files = [];
      for (let i = 0; i < extensions.length; ++i)
         this.files = this.files.concat(searchDirectory(this.baseDirectory + "/*" + extensions[i], true /*recursive*/ ));

   };

   this.baseDirectory = "";
   this.files = [];
   this.index = [];

   if (dirPath != undefined)
      this.regenerate(dirPath, extensions, verbose);

   if (verbose) {
      console.writeln("<end><cbr>" + this.files.length + " file(s) found:");
      for (let i = 0; i < this.files.length; ++i)
         console.writeln(this.files[i]);
   }
}
FileList.prototype = new Object;

// ----------------------------------------------------------------------------

function getFiles() {
   let gdd = new GetDirectoryDialog;
   let files = [];

   gdd.caption = "Select your directory";
   if (gdd.execute()) {
      let rootDir = gdd.directory;

      // get the list of compatible file extensions
      let openFileSupport = new OpenFileDialog;
      openFileSupport.loadImageFilters();
      let filters = openFileSupport.filters[0]; // all known format
      filters.shift();
      filters = filters.concat(filters.map(f => (f.toUpperCase())));

      // perform the search
      let filesFound = 0;
      let addedFiles = 0;
      let L = new FileList(rootDir, filters, false /*verbose*/ );
      L.files.forEach(filePath => {
         filesFound++;
         files.push(filePath);
      });
   }

   return files;
}

// ----------------------------------------------------------------------------

function main() {
   let files = getFiles();

   for (i = 0; i < files.length; ++i) {
      let fPath = files[i];
      let value = "";
      let w = ImageWindow.open(fPath);
      let k = w[0].keywords;
      for (let j = 0; j < k.length; ++j) {
         k[j].trim();
         if (k[j].name == keywordName) {
            if (parseAsNumber) {
               let val = parseFloat(k[j].strippedValue);
               if (isNaN(val))
                  continue;
               val = Math.round(val / precision) * precision;
               value = "" + val;
            } else {
               value = k[j].strippedValue
            }
            break;
         }
      }
      if (value != "") {
         let fName = File.extractName(fPath)
         let newfilePath;
         if (addAsPrefix)
            newfilePath = fPath.replace(fName, keywordName + "_" + value + "_" + fName);
         else
            newfilePath = fPath.replace(fName, fName + "_" + keywordName + "_" + value);

         if (renameOriginals) {
            w[0].forceClose();
            File.moveFile(newfilePath, fPath);
         } else {
            console.noteln("saving file ", fPath, " to ", newfilePath);
            w[0].saveAs(newfilePath,
               false /*queryOptions*/ ,
               false /*allowMessages*/ ,
               false /*strict*/ ,
               true /*verifyOverwrite*/ );
            w[0].forceClose();
         }
      } else {
         w[0].forceClose();
      }
   }
}

main()